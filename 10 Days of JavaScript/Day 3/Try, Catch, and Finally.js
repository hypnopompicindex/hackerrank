/*
 * Complete the reverseString function
 * Use console.log() to print to stdout.
 */
function reverseString(s) {
  try {
    let splitString = s.split("");
    let reverseString = splitString.reverse();
    let joinString = reverseString.join("");
    console.log(joinString);
  } catch (error) {
    console.log(error.message);
    console.log(s);
  }
}
